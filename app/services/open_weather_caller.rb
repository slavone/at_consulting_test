require 'open_weather'

module OpenWeatherCaller
  DEFAULT_OPTIONS = { APPID: Rails.application.secrets.open_weather_api_key,
                      lang: 'ru',
                      units: 'metric' }

  class << self
    def current_weather_at(place)
      map_result OpenWeather::Current.geocode(place.lat, place.lon, DEFAULT_OPTIONS)
    end

    def map_result(result_hash)
      {
        description: result_hash["weather"].first['description'],
        temp: result_hash.dig('main','temp'),
        pressure: result_hash.dig('main','pressure'),
        temp_min: result_hash.dig('main','temp_min'),
        temp_max: result_hash.dig('main','temp_max')
      }
    end
  end
end
