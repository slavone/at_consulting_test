class PlacesController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_user, except: [:index, :show]

  def index
    @places = Place.all
  end

  def new
    @place = Place.new
  end

  def show
    @place = Place.includes(:weather).find(params[:id])
    @weather = @place.weather
    if @weather
      unless @weather.updated_at > 10.minutes.ago
        @weather.update(OpenWeatherCaller.current_weather_at @place)
      end
    else
      @weather = @place.create_weather OpenWeatherCaller.current_weather_at @place
    end
  end

  def create
    @place = Place.new place_params
    if @place.save
      redirect_to places_path
    else
      render :new
    end
  end

  def update
    @place = Place.find params[:id]
    if @place.update place_params
      redirect_to places_path
    else
      render :edit
    end
  end

  def edit
    @place = Place.find params[:id]
  end

  def destroy
    @place = Place.find params[:id]
    @place.delete
    redirect_to places_path
  end

  private

  def authorize_user
    unless current_user.role == :admin
      redirect_to places_path
    end
  end

  def place_params
    params.require(:place).permit :lat, :lon, :address
  end
end
