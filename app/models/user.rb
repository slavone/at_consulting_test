class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :rememberable
  extend Enumerize
  enumerize :role, :in => { admin: 0, user: 1 }, scope: true
end
