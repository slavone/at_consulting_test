@initMap = ->
  placeLat = document.getElementById('place_lat')
  placeLon = document.getElementById('place_lon')

  geocoder = new google.maps.Geocoder

  initLatLng = { lat: parseFloat(placeLat.value), lng: parseFloat(placeLon.value) }
  if isNaN(initLatLng.lat) || isNaN(initLatLng.lng)
    initLatLng = { lat: 55.76, lng: 37.63 }

  map = new google.maps.Map document.getElementById('map'),
    center: initLatLng,
    zoom: 8

  marker = new google.maps.Marker
    position: initLatLng
    map: map

  if document.getElementById('map').dataset.editable == 'true'
    google.maps.event.addListener map, "click", (event) ->
      marker.setPosition(event.latLng)
      geocoder.geocode {'location': event.latLng}, (results, status)->
        if status == google.maps.GeocoderStatus.OK
          document.getElementById('place_address').value = results[0].formatted_address

      placeLat.value = event.latLng.lat()
      placeLon.value = event.latLng.lng()

    $("#place_address").on 'change', ->
      geocoder.geocode {'address': this.value }, (results, status)->
        if status == google.maps.GeocoderStatus.OK
          location = results[0].geometry.location
          marker.setPosition location
          placeLat.value = location.lat()
          placeLon.value = location.lng()
          map.setCenter location
