class CreateWeathers < ActiveRecord::Migration[5.0]
  def change
    create_table :weathers do |t|
      t.references :place, foreign_key: true
      t.string :description
      t.decimal :temp
      t.integer :pressure
      t.decimal :temp_min
      t.decimal :temp_max

      t.timestamps
    end
  end
end
